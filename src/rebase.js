import Rebase from 're-base';
import firebase from 'firebase';

var app = firebase.initializeApp({
  apiKey: "AIzaSyAYKszhvHIrDdyb1TRutHXODy1Bdam_Siw",
  authDomain: "younoob-2a180.firebaseapp.com",
  databaseURL: "https://younoob-2a180.firebaseio.com",
  projectId: "younoob-2a180",
  storageBucket: "younoob-2a180.appspot.com",
  messagingSenderId: "28060754049"
});

var base = Rebase.createClass(app.database());

export const provider = new firebase.auth.GoogleAuthProvider();
export default base
